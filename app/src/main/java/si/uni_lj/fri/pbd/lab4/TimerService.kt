package si.uni_lj.fri.pbd.lab4

import android.app.*
import android.content.Intent
import android.graphics.Color
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat

class TimerService : Service() {
    companion object {
        private val TAG: String? = TimerService::class.simpleName
        const val ACTION_STOP = "stop_service"
        const val ACTION_START = "start_service"
        private const val channelID = "background_timer"
        const val NOTIFICATION_ID = 7
    }


    // Start and end times in milliseconds
    private var startTime: Long = 0
    private var endTime: Long = 0

    /**
     * @return whether the timer is running
     */
    // Is the service tracking time?
    var isTimerRunning = false
        private set


    private val serviceBinder = RunServiceBinder()


    override fun onCreate() {
        Log.d(TAG, "Creating service")

        // TODO: set startTime, endTime, isTimerRunning to default values

        createNotificationChannel()
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        Log.d(TAG, "Starting service")

        if(intent.action == ACTION_STOP) {
            stopForeground(true)
            stopSelf()
        }

        return START_STICKY
    }

    override fun onBind(intent: Intent): IBinder? {
        Log.d(TAG, "Binding service")
        return serviceBinder
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "Destroying service")
    }

    fun startTimer() {
        if (!isTimerRunning) {
            startTime = System.currentTimeMillis()
            isTimerRunning = true
        } else {
            Log.e(MainActivity.TAG, "startTimer request for an already running timer")
        }
    }

    /**
     * Stops the timer
     */
    fun stopTimer() {
        if (isTimerRunning) {
            endTime = System.currentTimeMillis()
            isTimerRunning = false
        } else {
            Log.e(MainActivity.TAG, "stopTimer request for a timer that isn't running")
        }
    }

    /**
     * Returns the  elapsed time
     *
     * @return the elapsed time in seconds
     */
    fun elapsedTime(): Long {
        // If the timer is running, the end time will be zero
        return if (endTime > startTime) (endTime - startTime) / 1000 else (System.currentTimeMillis() - startTime) / 1000
    }

    inner class RunServiceBinder: Binder() {
        val service: TimerService
            get() = this@TimerService
    }

    fun foreground() {
        startForeground(NOTIFICATION_ID, createNotification())
    }

    fun background() {
        stopForeground(true)
    }
    /**
     * Creates a notification for placing the service into the foreground
     *
     * @return a notification for interacting with the service when in the foreground
     */
     private fun createNotification(): Notification {

        val actionIntent = Intent(this, TimerService::class.java)
        actionIntent.action = ACTION_STOP

        val actionPendingIntent = PendingIntent.getService(this, 0,
            actionIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val builder = NotificationCompat.Builder(this, channelID)
                .setContentTitle(getString(R.string.notif_title))
                .setContentText(getString(R.string.notif_text))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setChannelId(channelID)
                .addAction(android.R.drawable.ic_media_pause, "Stop", actionPendingIntent)

        val resultIntent = Intent(this, MainActivity::class.java)
        val resultPendingIntent = PendingIntent.getActivity(this, 0, resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT)

        builder.setContentIntent(resultPendingIntent)
        return builder.build()
    }
     private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT < 26) {
            return
        } else {
            val channel = NotificationChannel(channelID, getString(R.string.channel_name), NotificationManager.IMPORTANCE_LOW)
            channel.description = getString(R.string.channel_desc)
            channel.enableLights(true)
            channel.lightColor = Color.RED
            channel.enableVibration(true)
            val managerCompat = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            managerCompat.createNotificationChannel(channel)
        }
    }

}